import time
import psutil
from fastapi import FastAPI

app    = FastAPI()

@app.get("/ping")
def read_root():
    return {"ping": "pong"}

@app.get("/uptime")
async def uptime():
    uptime_seconds = int(time.monotonic())
    return {"uptime_seconds": uptime_seconds}

@app.get("/disk")
async def get_disk():
    disk_usage = psutil.disk_usage('/')
    return {
        "total": disk_usage.total,
        "available": disk_usage.free
    }

@app.get("/memory")
async def get_memory():
    mem_usage = psutil.virtual_memory()
    return {
        "total": mem_usage.total,
        "available": mem_usage.available
    }
