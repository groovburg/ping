**This example show how working with tag and CI/CD, that creating a new docker image every time when new tag created. And pushing new docker image to docker hub.**

For testing this project you need do next steps:


1. Clone this repository by 

   `git clone `

2. Fill env file from env.example

3. Star docker-compose from download project path  

   `docker-compose up`

4. Browse to http://0.0.0.0:5000/ping

- If you look in file **main.py** you can se ther some _async's_, that _async_ allow you to check 
different parameters like: used memory, disk space, uptime 
you can change this parameters in `http://0.0.0.0:5000/"type here parameter"`

- For upload to your docker hub or Artifactory you need to change Variables in your  in - Setting > CI/CD > Variables
